﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _160820_
{
    // Attribute -- sticker 
    [XmlRoot("Danny")]
    public class Person
    {
        [XmlElement("first-name")]
        public string FirstName { get; set; }
        public string LastName { get; set; }

        private int code = 1;
        protected string neighebrhood = "ofakim";
        internal int cars = 4;
        public static int numberStatic = 4;

        public int Age = 5;
        [XmlAttribute("time-creation")]
        public string IAMAttribute = DateTime.Now.ToString();

        // parametersless ctor for deserialization]
        // deserilization:
        // 1. create an empty object
        // 2. populate fiels from the string [byte]
        public Person()
        {

        }

        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
    }
}
