﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _160820_
{
    class Program
    {

        static void Main(string[] args)
        {
            Person danny_shovevany = new Person("Danni", "Shovevany");

            // 1 - serialization
            // turn p1 into string [bytes]

            //XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Person));

            // Stream -- data transportation -- read or write

            //StringWriter stringWriter = new StringWriter();
            //myXmlSerializer.Serialize(stringWriter, danny_shovevany);
            //string res = stringWriter.ToString();

            ////string result = outStream.Write()

            //// creating new file stream
            //using (Stream file = new FileStream(@"..\..\xmlfile.xml", FileMode.Create))
            //{
            //    myXmlSerializer.Serialize(file, danny_shovevany);

            //} //closing file stream

            //// 2 - deserialization
            //Person person_from_file = null;
            //// turn the string [bytes] back into danny shovevani
            //using (Stream file = new FileStream(@"..\..\xmlfile.xml", FileMode.Open))
            //{
            //    person_from_file = myXmlSerializer.Deserialize(file) as Person;

            //} //closing file stream

            // --------------------- Array
            //Person[] pArray =
            //{
            //    new Person("number 1", "the best!"),
            //    new Person("zehavi", "python"),
            //    new Person("bond", "java"),
            //    new Person("james", "c#")
            //};

            XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Person[]));

            // creating new file stream
            //using (Stream file = new FileStream(@"..\..\xmlfile_array.xml", FileMode.Create))
            //{
            //    myXmlSerializer.Serialize(file, pArray);

            //} //closing file stream

            //XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Person[]));

            // creating new file stream
            Person[] pArray_read = null;
            using (Stream file = new FileStream(@"..\..\xmlfile_array.xml", FileMode.Open))
            {
                pArray_read = (Person[])myXmlSerializer.Deserialize(file);

            } //closing file stream
        }
    }
}
